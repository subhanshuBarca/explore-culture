import React from 'react';
import './App.css';
import './assets/scss/style.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/header';
import Homepage from './pages/homepage';
import Compare from './pages/compare';
import Footer from './components/footer';
import {Route} from 'react-router-dom';
function App() {
  return (
    <div className="App">
     <Header />
         <Route exact path="/" component={Homepage} />
         <Route exact path="/compare" component={Compare} />
     <Footer/>
    </div>
  );
}

export default App;
