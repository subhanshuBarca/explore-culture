import React, { Component } from 'react';

class comparison extends Component {
    render() {
        const countryName = this.props.countryData;
        return (
            <section className="_comparison">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="comparison__table">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>
                                                <label className="mr-2 mb-0">Country: </label>
                                                <select name="country" id="country">
                                                    {countryName.map((name, index) => {
                                                        return (
                                                            <option value={index} key={'countryName_' + index}>{name.name}</option>
                                                        )
                                                    })
                                                    }
                                                </select>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

}

export default comparison;