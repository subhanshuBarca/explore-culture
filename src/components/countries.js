import React, { Component } from "react";
export class Countries extends Component {
  render() {
    const cardList = this.props.blogData.BlogCardListing;
    return (
      <section className="_countries section__my">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section"> 
                <div className="section__title">{this.props.blogData.Heading.Title}</div>
              </div>
            </div>
            {cardList.map((cardData, index) => {
              return (
                <div className="col-lg-4" key={'countryCard_' +index}>
                  <div className="countryCard position-relative borderRadius__common">
                    <div className="countryCard__picture">
                      <img src={'http://localhost:1337' + cardData.Image.url} className="w-100 h-100 objectFit__cover borderRadius__common" alt="explore culture" />
                    </div>
                    <div className="countryCard__title">
                      {cardData.Title}
                    </div>
                  </div>
                </div>
              )
            })}

          </div>
        </div>
      </section>
    );
  }
}

export default Countries;
