import React, { Component } from "react";

export class Culture extends Component {
  render() {
    const homeCulture = this.props.cultureData.Wrapper;
    return (
      <section className="_culture section__my">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="section">
                <div className="section__title">
                  {this.props.cultureData.Heading}
                </div>
              </div>
            </div>
          </div>
          {homeCulture.map((culture, index) => {
            return (
              <div className={"row align-items-center " + (index % 2 !== 0 ? 'flex-row-reverse' : '')} key={'culture_' + index}>
                <div className="col-lg-6">
                  <div className="culture__picture">
                    <img src={'http://localhost:1337' + culture.Image.url} className="img-fluid w-100 h-100 objectFit__cover" alt="explore-culture" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="culture__title text-left">
                    {culture.Title}
                  </div>
                  <div className="culture__description text-left">
                    {culture.Description}
                  </div>
                </div>
              </div>
            )
          })
          }
        </div>
      </section>
    );
  }
}

export default Culture;
