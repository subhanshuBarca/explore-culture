import React, { Component } from "react";
export class footer extends Component {
  render() {
    return (
      <footer className="_footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <ul className="p-0 text-center list-unstyled">
                <li className="secondary__nav-item">Travel</li>
                <li className="secondary__nav-item">Explore food</li>
                <li className="secondary__nav-item">Meet People</li>
                <li className="secondary__nav-item">Compare Cities</li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default footer;
