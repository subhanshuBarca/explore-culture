import React, { Component } from "react";
import {
  Link
} from "react-router-dom";
export class header extends Component {
  state = {
    activeClass: ''
  }
  
  componentDidMount() {
    window.addEventListener('scroll', () => {
      let activeClass = '_header__scroll';
      if (window.scrollY === 0) {
        activeClass = '';
      }
      this.setState({ activeClass });
    });
  }
  render() {
    return (
      <header className={`_header ${this.state.activeClass}`}>
        <div className="desktop_header d-lg-block d-md-none d-none">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <nav className="d-flex align-items-center justify-content-between">
                  <div className="logo">
                    <span className="logo__e logo__text">
                      E 
                    </span>
                    <span className="logo__/ logo__text">
                      /
                    </span>
                    <span className="logo__c logo__text">
                      C
                    </span>
                  </div>
                  <ul className="navigation list-unstyled d-flex m-0">
                    <li className="navigation__item pr-4"><Link to="/">Home</Link></li>
                    <li className="navigation__item px-4"><Link to="/travel">Travel</Link></li>
                    <li className="navigation__item px-4"><Link to="/compare">Compare Cities</Link></li>
                    <li className="navigation__item pl-4"><Link to="/job-hunt">Job Hunt</Link></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default header;
