import React, { Component } from "react";
export class HomeHero extends Component {
  render() {
    const sectionStyle = {
      backgroundImage: `url(http://localhost:1337${this.props.banner.Image.url})`
    }
    return (
      <section className="_hero" style={sectionStyle}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="hero--content">
                <div className="hero--title">{this.props.banner.Title}</div>
                <div className="hero--description">
                   {this.props.banner.Subtitle}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default HomeHero;
