import React, { Component } from 'react';
import Comparison from '../components/comparison';
import axios from 'axios';
import Exloader from '../components/exLoader';

class compare extends Component {
    state = {
        loading: true,
        countryData: []
    };

    render() {
        const { loading } = this.state;
        return (loading ? (<Exloader/>)
            : (
                <div className="compare">
                    <Comparison countryData={this.state.countryData} />

                </div>
            ));
    }
    componentDidMount() {
        axios.get('https://restcountries.eu/rest/v2/all')
            .then(response => {
                this.setState({ countryData: response.data, loading: false });
            })
            .catch(error => {
                console.log(error);
            });
    }
    timer = () => setTimeout(() => {
        this.setState({ loading: false })
    }, 2300);
}

export default compare;