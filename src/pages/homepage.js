import React, { Component } from 'react';
import HomeHero from '../components/homeHero';
import Countries from '../components/countries';
import Culture from '../components/culture';
import Exloader from '../components/exLoader';
import axios from 'axios';
export class Homepage extends Component {
  state = {
    loading: true,
    bannerData: {},
    blogCard: {},
    culture: {}
  };
  render() {
    const { loading } = this.state;
    return (loading ? (<Exloader
   />)
      : (
        <div className="homePage">
          <HomeHero banner={this.state.bannerData} />
          <Countries blogData={this.state.blogCard} />
          <Culture cultureData={this.state.culture} />
        </div>
      ));
  }
  componentDidMount() {
    axios.get('http://localhost:1337/homepage')
      .then(response => {
        this.setState({ bannerData: response.data.Banner, blogCard: response.data.BlogSection, culture: response.data.InternationalCulture, loading: false });
      })
      .catch(error => {
        console.log(error);
      });
  }
  componentWillUnmount() {
    clearTimeout(this.isLoading);
  }
  timer = () => setTimeout(() => {
    this.setState({ loading: true })
  }, 2300);
}


export default Homepage;
